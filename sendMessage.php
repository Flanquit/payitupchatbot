<?php

require_once './vendor/autoload.php';
require_once 'vendor/paynow/php-sdk/autoloader.php';

use Twilio\Rest\Client;

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();


// Your Account Sid and Auth Token from twilio.com/console
$sid    = getenv("AC8542de0c89a8a8701afc0cb9bd4938ac");
$token  = getenv("473d6a4cb22e792d52ac7220ba8bb6b8");
$twilio = new Client($sid, $token);

$codes = ["chocolate", "vanilla", "strawberry", "mint-chocolate-chip", "cookies-n-cream"];

$message = $twilio->messages
   ->create("whatsapp:".getenv("MY_WHATSAPP_NUMBER"),
       [
           "body" => "Your ice-cream code is ".$codes[rand(0,4)],
           "from" => "whatsapp:".getenv("TWILIO_WHATSAPP_NUMBER")
       ]
   );

print($message->sid);