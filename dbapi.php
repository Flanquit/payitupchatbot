<?php
@session_start();
$db = new PDO('mysql:host=localhost;dbname=bot;charset=utf8', 'root', '');

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//get all packages 


function get_all_packages()
{
     global $db;
  
    try {
        $stm = $db->prepare('SELECT * FROM product');
        $stm->execute();
        $rslt = $stm->fetchAll(PDO::FETCH_ASSOC);  
    } 
    catch (PDOExcepion $ex) {
        $rslt = $ex->getMessage();
    }
    return $rslt;
}

//get premium

function get_all_packages_id($package)
{
     global $db;
  
    try {
        $stm = $db->prepare('SELECT value FROM `product` WHERE `name` = ?');
        $stm->execute(array($package));
        $rslt = $stm->fetchAll(PDO::FETCH_ASSOC);  
    } 
    catch (PDOExcepion $ex) {
        $rslt = $ex->getMessage();
    }
    return $rslt;
}

//print_r(get_all_packages_id('Premium'));



